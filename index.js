// Example express application adding the parse-server module to expose Parse
// compatible API routes.

var express = require('express');
const compression = require('compression');
var ParseServer = require('parse-server').ParseServer;
var path = require('path');
var ParseDashboard = require('parse-dashboard');
const cors = require('cors');
const fs = require('fs');
const { spawn } = require('child_process');
var RedisCacheAdapter = require('parse-server').RedisCacheAdapter;
var FSFilesAdapter = require('@parse/fs-files-adapter');
require('dotenv').config()

var fsAdapter = new FSFilesAdapter();
var redisCache = new RedisCacheAdapter({ url: process.env.REDIS_URL });

const testApi = require('./api/test')
const content_api = require('./api/content')
const user_api = require('./api/user')

const FULL_URL = process.env.PRODUCTION === 'true' ? `${process.env.SERVER_URL}:${process.env.PORT}${process.env.PARSE_MOUNT}` : 'http://localhost:1337/parse'
const apiVersion = 'v1'


var databaseUri = process.env.DATABASE_URI || process.env.MONGODB_URI;

if (!databaseUri) {
    console.log('DATABASE_URI not specified, falling back to localhost.');
}

var allowInsecureHTTP = true;
var dashboard = new ParseDashboard({
    "allowInsecureHTTP": true,
    "apps": [{
        "serverURL": FULL_URL,
        "appId": process.env.APP_ID,
        "masterKey": process.env.MASTER_KEY,
        "appName": process.env.APP_NAME
    }],
    "users": [{
        "user": process.env.ADMIN_USERNAME,
        "pass": process.env.ADMIN_PASSWORD
    }]
}, { allowInsecureHTTP: allowInsecureHTTP });

var api = new ParseServer({
    databaseURI: databaseUri || 'mongodb://localhost:27017/dev',
    cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
    appId: process.env.APP_ID || 'myAppId',
    masterKey: process.env.MASTER_KEY || '', //Add your master key here. Keep it secret!
    serverURL: FULL_URL,
    javascriptKey: process.env.JS_KEY,
    restAPIKey: process.env.REST_KEY,
    liveQuery: {
        classNames: ["Content", "User"] // List of classes to support for query subscriptions
    },
    filesAdapter: fsAdapter,
    cacheAdapter: redisCache,
    fileUpload: {
        enableForPublic: true,
        enableForAnonymousUser: true,
        enableForAuthenticatedUser: true,
    },
    maxUploadSize:"100mb",
});


var app = express();
app.use(compression());
app.use(express.json());
app.use(cors())
app.use('/parse-dashboard', dashboard);
app.use(express.static(__dirname + '/public'));

var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

app.use(`/api/${apiVersion}/test`, testApi);
app.use(`/api/${apiVersion}/content`, content_api);
app.use(`/api/${apiVersion}/user`, user_api);


app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '/public/index.html'));
});

app.post('/update', function (req, res) {
    const name = req.body.name
    const child = spawn('bash', ['./update', name]);
    child.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
    res.status(200).send('OK')
});


var port = process.env.PORT || 1337;
if (process.env.PRODUCTION === 'true') {
    const domainName = process.env.SERVER_URL.replace('https://', '')
    const privateKey = fs.readFileSync(`/etc/letsencrypt/live/${domainName}/privkey.pem`, 'utf8') || '';
    const certificate = fs.readFileSync(`/etc/letsencrypt/live/${domainName}/cert.pem`, 'utf8') || '';
    const ca = fs.readFileSync(`/etc/letsencrypt/live/${domainName}/chain.pem`, 'utf8') || '';
    const credentials = {
        key: privateKey,
        cert: certificate,
        ca: ca
    };
    var httpsServer = require('https').createServer(credentials, app)
    httpsServer.listen(port, function () {
        console.log('server running on port ' + port + '.');
    });
    ParseServer.createLiveQueryServer(httpsServer);

} else {
    var httpsServer = require('http').createServer(app)
    httpsServer.listen(port, function () {
        console.log('server running on port ' + port + '.');
    });
    ParseServer.createLiveQueryServer(httpsServer);
}
