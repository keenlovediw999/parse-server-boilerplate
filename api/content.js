var express = require('express');
const router = express.Router();


router.get('/', async function (req, res, next) {
    try {
        const Content = Parse.Object.extend("Content");
        const query = new Parse.Query(Content);
        query.limit(100000)
        const results = await query.find();
        res.send({ status: 'success', message: "ok", data: results })
    } catch (error) {
        res.status(400).json({ status: 'error', message: error.message });
    }
});

router.post('/', async function (req, res, next) {
    try {
        const Content = Parse.Object.extend("Content");
        const obj = new Content();
        obj.set('rateCount',0)
        const saveObj = await obj.save({
            ...req.body, expireDate: new Date()
        })
        res.send({ status: 'success', message: 'save done', object: saveObj });
    } catch (error) {
        res.status(400).json({ status: 'error', message: error.message });
    }

});


module.exports = router