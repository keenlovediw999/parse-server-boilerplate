Parse.Cloud.define('hello', function(req, res) {
  return 'Hi';
});


Parse.Cloud.afterSave("Content", async (request) => {
  const item = request.object
  if(item.get('rateCount') === 0){
    item.increment('rateCount',1)
    await item.save()
  }
  
});